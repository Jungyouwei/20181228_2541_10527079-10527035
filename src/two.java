public class two {
    public static void main(String[] args) {

        int[] array = {1, 2, 5, 3, 10, 21, 0};
        getSortD_X(array);
        getSortX_D(array);
    }

    public static void getSortD_X(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[i] > array[j]) {
                    int temp = array[i];

                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        System.out.print("?大到小：");

        for (int i = 0; i < array.length; i++) {
            if (i == array.length - 1) {
                System.out.println(array[i]);
            } else {
                System.out.print(array[i] + ",");
            }
        }
    }

    public static void getSortX_D(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int

                 j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }

        System.out.print("?小到大：");
        for (int i = 0; i < array.length; i++) {
            if (i == array.length - 1) {
                System.out.println(array[i]);

            } else {
                System.out.print(array[i] + ",");
            }
        }
    }
}
